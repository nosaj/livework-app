require('dotenv').config();
/* global require */
const webpack = require('webpack');
const clear = require('clear');

const log = require('debug')('build:log');
const buildError = require('debug')('build:error');
const buildWarn = require('debug')('build:warn');

// Parse args
const watch = process.argv.filter((val) => val === '--watch').length > 0;
const release = process.argv.filter((val) => val === '--release').length > 0;

const ENV = process.env.NODE_ENV || 'development';
const config = (ENV === 'production' || release) ?
  require('./config/webpack.production.config.js') :
  require('./config/webpack.config.js')

outputStartupSuccess();
const compiler = webpack(config);

if (watch) {
  compiler.watch({}, handleCompiled);
  return;
}

compiler.run(handleCompiled);

function handleCompiled(err, stats) {
  if (err) {
    // Throw for fatal errors
    buildError(err.details);
  }
  const info = stats.toJson();
  
  // Handle non-fatal errors and warnings
  if (stats.hasErrors()) {
    return info.errors.forEach(e => buildError(e));
  }
  if (stats.hasWarnings()) {
    return info.warnings.forEach(w => buildWarn(w));
  }
  
  clear(); // Clear the console of previous output to keep stuff clean
  log('🙌   Built in %sms', stats.endTime - stats.startTime);
}

function outputStartupSuccess() {
  clear();
  console.log('⚙️   Running first compilation...');
}

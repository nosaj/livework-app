require('dotenv').config();

const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

process.traceDeprecation = true;

const publicPath = `http://localhost:${process.env.PORT}/`;
module.exports = {
  context: path.resolve(__dirname, '../app'),
  entry: [
    'babel-polyfill', 
    'webpack-hot-middleware/client?reload=true', 
    path.resolve(__dirname, '../src/entry.js')
  ],
  output: {
    publicPath,
    path: path.resolve(__dirname, '../dist'),
    filename: 'app.js'
  },
  resolve: {
    mainFiles: ['index'],
    modules: [
      path.resolve(__dirname, '../node_modules'),
      path.resolve(__dirname, '../src')
    ],
    extensions: ['.js', '.jsx', '.json'],
  },
  module: {
    loaders: [
      {
        test: /\.(gif|jpe?g|png|svg)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: 'img/[name]-[hash].[ext]',
            }
          },
          {
            loader: 'image-webpack-loader',
            options: {
              mozjpeg: {
                progressive: true,
              },
              optipng: {
                optimizationLevel: 7
              }
            }
          }
        ],
      },
      {
        test: /\.json$/,
        loader: 'json-loader'
      },
      {
        test: /\.scss$/,
        use: [
          { loader: 'style-loader' },
          { loader: 'css-loader' },
          { loader: 'postcss-loader' },
          { loader: 'sass-loader' },
        ]
      },
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        query: {
          presets: [
              'react',
              ['env', {
                targets: {
                  browsers: ['last 2 major versions', 'safari >= 9']
                }
            }]
          ],
          plugins: ['babel-plugin-transform-object-rest-spread']
        }
      }
    ],
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('development'),
        PORT: JSON.stringify(process.env.PORT),
        MAPBOX_API_TOKEN: JSON.stringify(process.env.MAPBOX_API_TOKEN),
        APP_URL: JSON.stringify(process.env.APP_URL),
        npm_package_version: JSON.stringify(process.env.npm_package_version),
      }
    }),
  ]
};

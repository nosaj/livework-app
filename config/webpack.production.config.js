require('dotenv').config();
const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
  context: path.resolve(__dirname, '../app'),
  entry: ['babel-polyfill', path.resolve(__dirname, '../src/entry.js')],
  output: {
    publicPath: '/',
    path: path.resolve(__dirname, '../dist'),
    filename: 'app.js'
  },
  resolve: {
    mainFiles: ['index'],
    modules: [
      path.resolve(__dirname, '../node_modules'),
      path.resolve(__dirname, '../src')
    ],
    extensions: ['.js', '.jsx', '.json'],
  },
  module: {
    loaders: [
      {
        test: /\.(gif|jpe?g|png|svg)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: 'img/[name]-[hash].[ext]',
            }
          },
          {
            loader: 'image-webpack-loader',
            options: {
              mozjpeg: {
                progressive: true,
              },
              optipng: {
                optimizationLevel: 7
              }
            }
          }
        ],
      },
      {
        test: /\.json$/,
        loader: 'json-loader'
      },
      {
        test: /\.scss$/,
        loader: ExtractTextPlugin.extract({ fallback: 'style-loader', use: 'css-loader!postcss-loader!sass-loader' }),
      },
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        query: {
          presets: [
              'react',
              ['env', {
                targets: {
                  browsers: ['last 2 major versions', 'safari >= 9']
                }
            }]
          ],
          plugins: ['babel-plugin-transform-object-rest-spread']
        }
      }
    ],
  },
  plugins: [
    new ExtractTextPlugin('app.css'),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('production'),
        PORT: JSON.stringify(process.env.PORT),
        MAPBOX_API_TOKEN: JSON.stringify(process.env.MAPBOX_API_TOKEN),
        APP_URL: JSON.stringify(process.env.APP_URL),
        npm_package_version: JSON.stringify(process.env.npm_package_version),
      }
    }),
    new webpack.optimize.UglifyJsPlugin({
      sourceMap: true,
      compress: {
        warnings: false,
        comparisons: false,  // don't optimize comparisons
      },
    }),
  ],
};

const debug = require('debug')('app:api:getProperties');
const error = require('debug')('app:error:api:getProperties');
const { safeResult } = require('../helpers/db');
const { allAgents } = require('../db');

/**
 * GET: /api/agents
 * Return a array of all agents in the database
 */
function handleGetAgents(req, res) {
  allAgents()
    .then(agents => {
      const result = agents.map(a => safeResult(a, ['id']));
      res.json(result);
    })
    .catch(err => error(err));
}

module.exports = handleGetAgents;

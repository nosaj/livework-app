const debug = require('debug')('app:api:getLists');
const error = require('debug')('app:error:api:getLists');
const { listsForEmail } = require('../db');
const { safeResult } = require('../helpers/db');

const handleGetLists = (req, res) => {
  const { query } = req;
  listsForEmail(query.email)
    .then(lists => {
      if (! lists.length) {
        res.json([]);
        return;
      }
      res.json(lists.map(l => safeResult(l, ['id'])));
    })
}

module.exports = handleGetLists;

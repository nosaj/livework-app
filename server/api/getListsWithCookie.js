const debug = require('debug')('app:api:getLists');
const error = require('debug')('app:error:api:getLists');
const { listsForEmail } = require('../db');
const { safeResult } = require('../helpers/db');

const handleGetListsWithCookie = (req, res) => {
  const { cookies } = req;
  if (! cookies.email) {
    res.json([]);
    return;
  }
  listsForEmail(cookies.email)
    .then(lists => {
      if (! lists.length) {
        res.json([]);
        return;
      }
      res.json(lists.map(l => safeResult(l, ['id'])));
    })
}

module.exports = handleGetListsWithCookie;

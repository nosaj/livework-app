const debug = require('debug')('app:api:getProperties');
const error = require('debug')('app:error:api:getProperties');
const { safeResult } = require('../helpers/db');
const { allPropertiesWithAgents } = require('../db');

/**
 * GET: /api/properties
 * Return a array of all properties in the database
 */
function handleGetProperties(req, res) {
  allPropertiesWithAgents()
    .then(properties => {
      let results = properties.map(p => safeResult(p, ['id']));
      // Reverse the order of pictures so that they are in the same order as
      // the source
      results = results.map(r => {
        if (Array.isArray(r.pictures)) {
          r.pictures = r.pictures.reduce((acc, val) => {
            acc.unshift(val)
            return acc;
          }, []);
        }
        return r;
      });
      res.json(results);
    })
    .catch(err => {
      res.status(500).json({});
      error(err);
    });
}

module.exports = handleGetProperties;

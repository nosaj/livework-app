const debug = require('debug')('app:api:getRedirectToProperty');
const error = require('debug')('app:error:api:getRedirectToProperty');
const { singleProperty } = require('../db');


// This route will just forward requests to the right page on agent's site.
// We do it this way so that requests can be tracked.
const getRedirectToProperty = (req, res) => {
  const { uuid } = req.params;
  singleProperty(uuid)
    .then(property => {
      debug('Redirect to agent: %s, %s', uuid, property.url)
      res.redirect(301, property.url);
    })
    .catch(err => {
      error(err);
      res.status(500).json({});
    });
}

module.exports = getRedirectToProperty;

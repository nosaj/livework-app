const debug = require('debug')('app:api:getSearch');
const error = require('debug')('app:error:api:getSearch');
const { filterProperties } = require('../db');
const { safeResult } = require('../helpers/db');


function handleGetSearch(req, res) {
  const { query } = req;
  filterProperties(query)
    .then(rows => {
      res.json( {
        count: rows.length,
        results: rows.map(r => safeResult(r, ['id'])) 
      });
    })
    .catch(err => error(err));
}

module.exports = handleGetSearch;

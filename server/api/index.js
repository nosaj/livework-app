const { Router } = require('express');

const apiRouter = Router();

// Configure the routes
apiRouter.get('/properties', require('./getProperties'));
apiRouter.get('/agents', require('./getAgents'));
apiRouter.get('/search', require('./getSearch'));
apiRouter.get('/goto/:uuid', require('./getRedirectToProperty'));
apiRouter.get('/lists', require('./getLists'));
apiRouter.get('/lists/cookie', require('./getListsWithCookie'));
apiRouter.post('/lists', require('./postList'));
apiRouter.post('/bookings', require('./postBooking'));

module.exports = apiRouter;

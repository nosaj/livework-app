const debug = require('debug')('app:api:postBooking');
const error = require('debug')('app:error:api:postBooking');
const { isEmpty, validateEmail } = require('../helpers/data');
const { sendBookingEmailToAgent } = require('../email/senders');

const validateData = ({ listingTitle, listingUrl, availability, name, email, phone, agentId }) => {
  if (
    isEmpty(listingTitle) || 
    isEmpty(listingUrl) || 
    isEmpty(name) || 
    isEmpty(availability) || 
    isEmpty(email) || 
    isEmpty(agentId)
  ) {
    return { valid: false, emptyFields: true };
  }
  if (! validateEmail(email)) {
    return { valid: false, invalidEmail: true };
  }
  return { valid: true };
}

function postBooking(req, res) {
  const { 
    listingTitle, 
    listingUrl, 
    availability, 
    name, 
    email, 
    phone, 
    agentId 
  } = req.body;
  const validated = validateData(req.body);
  if (! validated.valid) {
    const errorRes = {
      success: false,
      error: true,
    };
    if (validated.emptyFields) {
      errorRes.message = 'missing fields';
    }
    if (validated.invalidEmail) {
      errorRes.message = 'invalid email';
    }
    res.status(400).json(errorRes);
    return;
  }
  sendBookingEmailToAgent(listingTitle, listingUrl, availability, name, email, phone, agentId)
    .then(bookingRes => {
      res.json({
        success: true,
        ref: `LDN-${bookingRes.ref}`,
      });
    })
    .catch(err => {
      if (err) {
        error(err);
      }
      res.status(500).json({
        error: true,
        message: 'Not Hot Dog',
      })
    });
}

module.exports = postBooking;

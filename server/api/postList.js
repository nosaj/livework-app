const debug = require('debug')('app:api:postList');
const error = require('debug')('app:error:api:postList');
const { addToList } = require('../db');
const { validateEmail } = require('../helpers/data');

const monthsToSeconds = months => months * (60 * 60 * 24 * 7 * 30);

const addEmailCookie = (email, res) => {
  res.cookie('email', email, { maxAge: monthsToSeconds(1), httpOnly: false });
}

function handlePostList(req, res) {
  const { body } = req;
  const { email, list, data } = body;
  if (! validateEmail(email)) {
    res.json({
      invalidEmail: true,
      email,
      added: false,
    });
    return;
  }
  addToList(list, email, data || {})
    .then(added => {
      addEmailCookie(email, res)
      res.json({ 
        invalidEmail: false, 
        added, 
        email,
      });
    })
    .catch(err => error(err));
}

module.exports = handlePostList;

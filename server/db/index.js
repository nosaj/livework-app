const debug = require('debug')('app:db');
const { Client } = require('pg');
const client = new Client();
const { 
  minPriceSQL,  
  maxPriceSQL,
  minSqftSQL,
  sharedSQL
} = require('../helpers/db');

const initClient = async () => {
  await client.connect();
  debug('Connected to database');
}

const allProperties = async () => {
  const { rows } = await client.query('SELECT * FROM properties');
  return rows;
}

const singleProperty = async uuid => {
  const { rows } = await client.query('SELECT * FROM properties WHERE properties.uuid = $1', [uuid]);
  return rows[0];
}

const singleAgent = async uuid => {
  const { rows } = await client.query('SELECT * FROM agents WHERE agents.uuid = $1', [uuid]);
  return rows[0];
}

const allAgents = async () => {
  const { rows } = await client.query('SELECT * FROM agents');
  return rows;
}

const allPropertiesWithAgents = async () => {
  const query = `SELECT 
    properties.*, 
    agents.uuid           AS agent_uuid, 
    agents.name           AS agent_name, 
    agents.logo_url       AS agent_logo,
    agents.url            AS agent_url,
    agents.booking_email  AS agent_email
    FROM properties INNER JOIN agents ON (properties.agent_id = agents.id);`
  const { rows } = await client.query(query);
  return rows;
}

/**
 * Filter properties, and return a subset that matches the filterSettings
 * @param {object} filterSettings
 * @param {number} filterSettings.min_price
 * @param {number} filterSettings.max_price
 * @param {number} filterSettings.min_sqft
 * @param {boolean} filterSettings.shared
 * @returns {Promise<array, error>}
 */
const filterProperties = async filterSettings => {
  try {
    const params = Object.keys(filterSettings);
    const whereSQL = [];
    params.forEach(p => {
      switch(p) {
        case 'min_price': whereSQL.push( minPriceSQL(filterSettings) ); return;
        case 'max_price': whereSQL.push( maxPriceSQL(filterSettings) ); return;
        case 'min_sqft': whereSQL.push( minSqftSQL(filterSettings) ); return;
        case 'shared': whereSQL.push( sharedSQL(filterSettings) ); return;
      }
    });
    const query = `SELECT * FROM properties WHERE ${whereSQL.join(' AND ')}`
    const { rows } = await client.query(query);
    return rows;
  } catch(err) {
    throw err;
  }
}

/**
 * Add email address to specified list
 * @param {string} list
 * @param {string} email
 * @param {object} ?data
 */
const addToList = async (list, email, data) => {
  try {
    const query = `
      INSERT INTO 
      email(email, list, data) 
      VALUES($1, $2, $3)
      ON CONFLICT (email, list) DO NOTHING
      `;
    await client.query(query, [email, list, data]);
    return true;
  } catch (err) {
    throw err;
  }
}

/**
 * Return all lists for email address
 * @param {string} email
 */
const listsForEmail = async email => {
  try {
    const query = `SELECT * FROM email WHERE email = '${email}'`;
    const { rows } = await client.query(query);
    return rows;
  } catch (err) {
    throw err;
  }
}

/**
 * Insert a metadata recording of a booking email to the database
 * @param {string} ref
 * @param {string} mailer_id
 * @param {number} agent_id
 */
const recordEmailRef = async (ref, mailer_id, agent_id) => {
  try {
    const query = `
      INSERT INTO 
      bookings(ref, mailer_id, agent_id)
      VALUES($1, $2, $3)`;
    await client.query(query, [ref, mailer_id, agent_id]);
    return true;
  } catch (err) {
    throw err;
  }
}

module.exports = {
  initClient,
  allProperties,
  singleProperty,
  singleAgent,
  allAgents,
  filterProperties,
  allPropertiesWithAgents,
  addToList,
  listsForEmail,
  recordEmailRef,
}

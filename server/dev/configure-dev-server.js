const pug                         = require('pug');
const path                        = require('path');
const webpack                     = require('webpack');
const webpackDevMiddleware        = require('webpack-dev-middleware');
const webpackHotMiddleware        = require('webpack-hot-middleware');
const webpackConfig               = require('../../config/webpack.config.js');
const { resolveRouteVariables }   = require('../helpers/routes');

module.exports = (app, appRoutes, viewsDir) => {
  const devOptions = {
    publicPath: webpackConfig.output.publicPath,
    noInfo: true,
    stats: {
      colors: true
    }
  };
  const hotOptions = {
    noInfo: true,
    publicPath: webpackConfig.output.publicPath,
  };
  const compiler = webpack(webpackConfig);
  const devMiddleware = webpackDevMiddleware(compiler, devOptions);
  const hotMiddleware = webpackHotMiddleware(compiler, hotOptions)
  app.use(devMiddleware);
  app.use(hotMiddleware);
  
  // Send the index.html file for all dynamic routes
  appRoutes.forEach(r => {
    app.get(r.route, (req, res) => {
      const mainView = path.join(viewsDir, 'main.pug');
      // Some route vars are packaged in a promise so they can be async
      resolveRouteVariables(req, r).then(variables => {
        const mainFile = pug.renderFile(mainView, variables);
        res.send(mainFile);
      });
    });
  })
}

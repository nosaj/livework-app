const composeBookingEmail = (listingTitle, listingUrl, availability, name, email, phone, ref) => {
  return {
    html: '',
    text: 
`Hi, 

This message was sent from liveworksearch.com, a search engine for Live/Work spaces in London. 
${name} has requested to view one of your properties:
----

I'd like to view a property you have listed on your website: ${listingTitle} (${listingUrl}). 
I'm available on ${availability}, my contact details are below:
Email: ${email}
${phone ? `Phone #: ${phone}` : ''}

Thanks,
${name}

If you reply to this email it will go to ${name}'s inbox.
Live/Work Search ref: ${ref}

[If you have any questions about these emails, or want to stop receiving them, email hi@nosaj.io.]
`
  }
}

module.exports = {
  composeBookingEmail,
}

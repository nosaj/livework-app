const debug = require('debug')('app:helpers:senders');
const { composeBookingEmail } = require('./composers');
const { recordEmailRef, singleAgent } = require('../db');

const { MAILGUN_KEY, MAILGUN_DOMAIN } = process.env;
const mailgun = require('mailgun-js')({ apiKey: MAILGUN_KEY, domain: MAILGUN_DOMAIN });

const generateRef = () => {
  const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
  let refStr = '';
  for (let i = 0; i < 6; i++) {
    refStr += characters.charAt(
      Math.floor(
        Math.random() * characters.length
      )
    );
  }
  return refStr;
}

const fireMailGun = (mailData, ref, agentId) => new Promise(resolve => {
  mailgun.messages().send(mailData, (err, body) => {
    if (err) {
      throw err;
    }
    recordEmailRef(ref, body.id, agentId);
    resolve({ ref });
  })
});

const sendBookingEmailToAgent = (listingTitle, listingUrl, availability, name, email, phone, agentUUID) => {
  if (! agentUUID) {
    return Promise.reject(new Error(`Cannot send email, agentUUID not specified listing: ${listingUrl}`));
  }
  
  return singleAgent(agentUUID)
    .then(agent => {
      const agentEmail = agent.booking_email;
      const userEmail = `${name} <${email}>`;
      const ref = generateRef();
      const emailBody = composeBookingEmail(listingTitle, listingUrl, availability, name, email, phone, ref);
      const mailData = {
        from: 'Live/Work Search <no-reply@liveworksearch.com>',
        to: agentEmail,
        'h:Reply-To': userEmail,
        subject: `Viewing request from liveworksearch.com (${ref})`,
        text: emailBody.text,
      }
      return fireMailGun(mailData, ref, agent.id);
    });
  
}

module.exports = { sendBookingEmailToAgent };

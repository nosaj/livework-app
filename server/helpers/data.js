const debug = require('debug')('app:helpers:data');

const isEmpty = predicate => predicate === '';

const validateEmail = emailStr => {
  if (! emailStr) {
    return false;
  }
  const regexp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return regexp.test(emailStr);
}

module.exports = {
  isEmpty,
  validateEmail,
};

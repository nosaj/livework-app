const debug = require('debug')('app:helpers:db');

/**
 * Safe result object - remove fields from the result object
 * @param {object} obj - the result object
 * @param {array} fields
 */
const safeResult = (obj, fields) => {
  fields.forEach(f => {
    delete obj[f]
  });
  return obj;
}

// Generate SQL WHERE clauses fir different facets of search
const minPriceSQL = filters => `week_price >= ${filters.min_price}`;
const maxPriceSQL = filters => `week_price <= ${filters.max_price}`;
const minSqftSQL = filters => `dimensions_sqft >= ${filters.min_sqft}`;
const sharedSQL = filters => `shared = ${filters.shared}`;

module.exports = { 
  safeResult,
  minPriceSQL,
  maxPriceSQL,
  minSqftSQL,
  sharedSQL,
};

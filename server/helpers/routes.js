const debug = require('debug')('app:helpers:routes');

module.exports = { resolveRouteVariables }

/**
 * Resolve Route Variables
 *
 * @param {Object} req - HTTP request object
 * @param {Object} route
 * @param {String} route.route
 * @param {Function || Promise} route.variables
 * @return {Promise} 
 */
function resolveRouteVariables(req, route) {
  const args = Object.assign({}, req.params, req.query);
  if (isPromise(route.variables())) {
    return route.variables(args);
  }
  // For non promises, just resolve immediately
  return Promise.resolve(route.variables(args));
}

function isPromise(predicate) {
  return predicate instanceof Promise;
}

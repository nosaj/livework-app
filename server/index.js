require('dotenv').config();
const pug                       = require('pug');
const path                      = require('path');
const debug                     = require('debug')('app:server');
const express                   = require('express');
const bodyParser                = require('body-parser');
const cookieParser              = require('cookie-parser');
const compression               = require('compression');
const configureDevServer        = require('./dev/configure-dev-server');
const { resolveRouteVariables } = require('./helpers/routes');

// Read .env variables
const ENV           = process.env.NODE_ENV || 'development';
const PORT          = process.env.PORT || 8181;

// Where the app will be compiled to
const staticRoot = path.join(__dirname, '..', 'dist');

// Where the app will serve aassets from
const assetsRoot = path.join(__dirname, '/static');

// The dynamic views dir (these are views served with the server)
const viewsDir = path.join(__dirname, 'views');

const appRoutes = require('./routes');

// Connect to the database
require('./db').initClient();

const app = express();

// Parse body of JSON requests
app.use( bodyParser.json() );
// Parse cookies as JSON
app.use( cookieParser() );

// Serve static assets
app.use('/assets', express.static(assetsRoot));

// Some middleware is only needed in production
if (ENV === 'production') {
  // Tell express where the base should be for static files,
  // like images, js, css, default index.html etc
  app.use(express.static(staticRoot));
  // Compress for fewer bytes down the wire
  app.use( compression() );
  // Always send the index file for routes in the react app. 
  // Let React handle the routing
  appRoutes.forEach(r => {
    app.get(r.route, (req, res) => {
      const mainView = path.join(viewsDir, 'main.pug');
      // Some route vars are packaged in a promise so they can be async
      resolveRouteVariables(req, r).then(variables => {
        const mainFile = pug.renderFile(mainView, variables);
        res.send(mainFile);
      });
    });
  })
}

if (ENV === 'development') {
  configureDevServer(app, appRoutes, viewsDir);
}

// Open up CORS
app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'POST, GET, PUT, OPTIONS');
  next();
});

// Configure the API
app.use('/api', require('./api'));

app.listen(PORT, () => {
  debug(`🌈  Listening on PORT ${PORT} ENV: ${ENV}`);
});

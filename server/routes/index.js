const debug = require('debug')('app:routes');
const { singleProperty } = require('../db');
const { APP_URL } = process.env;

module.exports = [
  {
    route: '/',
    variables: () => ({
      title: 'Live/Work spaces for rent in London',
      ogDescription: 'See all Live/Work spaces in and around London on one map.',
      ogImage: `${APP_URL}/assets/og/livework-og.png`,
      themeColor: '#f6e75e',
    }),
  },
  {
    route: '/properties/:uuid',
    variables: args => new Promise(resolve => {
      const defaultData = {
        title: 'Live/Work spaces for rent in London',
        ogDescription: 'See all Live/Work spaces in and around London on one map.',
        ogImage: `${APP_URL}/assets/og/livework-og.png`,
        themeColor: '#f6e75e',
      };
      if (! args || ! args.uuid) {
        resolve(defaultData);
        return;
      }
      singleProperty(args.uuid).then(property => {
        defaultData.title = property.listing_title;
        resolve(defaultData);
      })
    })
  },
];

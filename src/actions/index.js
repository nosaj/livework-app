import * as types from 'constants/actionTypes';


// map.focusProperty
export const focusProperty = property => ({
  type: types.FOCUS_PROPERTY,
  property,
});

// map
export const mapLoaded = () => ({
  type: types.MAP_LOADED,
});

// app.modal
export const showModal = children => ({
  type: types.SHOW_MODAL,
  children
})
export const hideModal = () => ({
  type: types.HIDE_MODAL,
})

// app.emailSignup
export const hideEmailSignup = () => ({
  type: types.HIDE_EMAIL_SIGNUP,
});

export const showEmailSignup = () => ({
  type: types.SHOW_EMAIL_SIGNUP,
});

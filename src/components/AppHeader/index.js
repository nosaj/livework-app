import React from 'react';
import { version } from 'constants/app';

import './app-header.scss';

const Logo = () => (
  <h1 className="app-header__logo">LiveWorkSearch <span className="version">v.{version}</span></h1>
);

const Menu = () => (
  <div className="app-header__menu"></div>
)

class AppHeader extends React.Component {
  render() {
    return (
      <div className="app-header">
        <Logo />
        <Menu />
      </div>
    );
  }
}

export default AppHeader;

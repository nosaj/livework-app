import React from 'react';
import PropTypes from 'prop-types';

import './binary-switch.scss';

const BinarySwitch = ({a, b, invalid}) => (
  <div className={`binary-switch ${invalid ? 'binary-switch--invalid' : ''}`}>
    <button 
      onClick={() => a.onSelect()}
      className={`
        binary-switch__switch 
        binary-switch__switch--a 
        ${a.selected ? 'binary-switch__switch--selected' : ''}
      `}>{a.label}</button>
    <button 
      onClick={() => b.onSelect()}
      className={`
        binary-switch__switch 
        binary-switch__switch--b
        ${b.selected ? 'binary-switch__switch--selected' : ''}
      `}>{b.label}</button>
  </div>
);

BinarySwitch.propTypes = {
  a: PropTypes.shape({ 
    label: PropTypes.string.isRequired,
    onSelect: PropTypes.func.isRequired,
    selected: PropTypes.bool,
  }),
  b: PropTypes.shape({ 
    label: PropTypes.string.isRequired,
    onSelect: PropTypes.func.isRequired,
    selected: PropTypes.bool,
  }),
  invalid: PropTypes.bool,
}

export default BinarySwitch;

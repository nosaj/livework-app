import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Input from 'components/Input';
import BinarySwitch from 'components/BinarySwitch';
import { postBooking } from 'services/api';

import './book-viewing-modal.scss';

class BookViewingModal extends Component {
  constructor() {
    super();
    this.state = {
      booked: false,
      ref: '',
      // Form state
      availability: null,
      name: null,
      email: null,
      phone: null,
      // Validation
      invalid: false,
      invalidFields: {
        availability: false,
        name: false,
        email: false,
      }
    }
  }
  
  render() {
    const { property, className } = this.props;
    const { booked, ref } = this.state;
    return booked ? this.renderSuccessMessage(ref, className) : this.renderForm(property, className);
  }
  
  renderForm(property, className) {
    return (
      <div className={`${className} book-viewing`}>
        <h1 className="book-viewing__title">When&#39;s good for you?</h1>
        <div className="book-viewing__form">
          <BinarySwitch
            invalid={this.state.invalidFields.availability}
            a={{
              label: 'Weekdays',
              onSelect: () => this.setState({ availability: 'Weekdays' }),
              selected: this.state.availability === 'Weekdays',
            }}
            b={{
              label: 'Weekends',
              onSelect: () => this.setState({ availability: 'Weekends' }),
              selected: this.state.availability === 'Weekends',
            }}
          />
          <Input 
            type="text"
            invalid={this.state.invalidFields.name}
            onChange={event => this.setState({ name: event.target.value })} 
            label="Your Name" 
          />
          <Input 
            type="email"
            invalidMessage="That doesn't look like a valid email address"
            invalid={this.state.invalidFields.email}
            onChange={event => this.setState({ email: event.target.value })} 
            label="Your Email Address" 
          />
          <Input 
            type="tel"
            onChange={event => this.setState({ phone: event.target.value })} 
            label="Your Phone No" 
            optional 
          />
          {this.state.invalid && (
            <div className="book-viewing__error">Make sure you fill out all required fields so the agent has enough information</div>
          )}
          <button 
            className="book-viewing__submit button button--black button--block"
            onClick={() => {
              if (this.validateForm()) {
                this.sendBookingRequest()
              }
            }}
            >Send</button>
        </div>
        <small className="book-viewing__smallprint">This will send a message to the agent with your details and availability. You details won&#39;t be stored.</small>
      </div>
    );
  }
  
  validateForm() {
    const {availability, name, email} = this.state;
    if (! availability || ! name || ! email) {
      this.setState({
        invalid: true,
        invalidFields: {
          availability: ! availability,
          name: ! name,
          email: ! email,
        }
      });
      return false;
    }
    this.setState({
      invalid: false,
      invalidFields: {
        availability: false,
        name: false,
        email: false,
      }
    })
    return true;
  }
  
  renderSuccessMessage(ref, className) {
    const { onModalClose } = this.props;
    return (
      <div className={`${className} booked-viewing`}>
        <p>Success! A message has been sent to the agent with your request. Your reference number is:</p>
        <h2>{ref}</h2>
        <small>Keep this reference safe</small>
        <div className="button button--black button--block" onClick={() => onModalClose()}>Back to the map</div>
      </div>
    );
  }
  
  sendBookingRequest() {
    const { availability, name, email, phone } = this.state;
    const { property: { listing_title, url, agent_uuid } } = this.props;
    postBooking(name, email, phone, availability, url, listing_title, agent_uuid)
    .then(booking => {
      if (booking.success) {
        this.setState({ booked: true, ref: booking.ref });
        return;
      }
      if (booking.error) {
        if (booking.message === 'invalid email') {
          this.setState({ 
            invalid: true, 
            invalidFields: {
              email: true,
              availability: false,
              name: false,
            }
          })
        }
      }
    });
  }
}

BookViewingModal.propTypes = {
  className: PropTypes.string,
  property: PropTypes.object,
  onModalClose: PropTypes.func,
};

export default BookViewingModal;

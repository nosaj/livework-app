import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { hideEmailSignup, showEmailSignup } from 'actions';
import { postToList } from 'services/api';
import X from 'components/X';

import './email-signup.scss';


class EmailSignupMessage extends Component {
  constructor() {
    super();
    this.state = {
      emailField: '',
      invalidEmail: false,
      pressedSend: false,
      // For letting the user select yes or no to a question
      selectedYes: false,
      selectedNo: false,
    }
  }
  
  componentWillMount() {
    this.props.onShow();
  }
  
  render() {
    const { pressedSend } = this.state;
    const { onHide, emailSignup } = this.props;
    const { isHidden } = emailSignup;
    return isHidden ? null : (
      <section className="email-signup">
          <div className="email-signup__messages">
          {pressedSend ? this.renderSentMessage() : this.renderMessages()}
          <div className="email-signup__close">
            <X onClick={() => onHide()} />
          </div>
        </div>
      </section>
    );
  }
  
  renderSentMessage() {
    return (
      <div className="email-signup__msg">
        <p>Thanks! You&#39;ll receive a confirmation email shortly <span role="img" aria-label="Happy face">😀</span></p>
      </div>
    );
  }
  
  renderMessages() {
    const { invalidEmail, selectedYes, selectedNo } = this.state;
    return (
      <div className="email-signup__msg">
        <p><span role="img" aria-label="Wave">👋</span> Hi I&#39;m Jason, I make LiveWorkSearch.com, a tool for finding Live/Work spaces for rent in London. I&#39;m in the process of adding new agents and properties as we speak. The goal is to list every available Live/Work space in London.</p>
        <p>Would you be interested in receiving an email no more than once a week containing a digest of new properties and agents?</p>
        {(selectedYes || selectedNo) || (
          <div className="email-signup__yes-no">
            <button
              className="button button--positive button--round"
              onClick={() => this.handleQuestionResponse('yes')}
            >Yes</button>
            <button
              className="button button--neutral button--round"
              onClick={() => this.handleQuestionResponse('no')}
            >No</button>
          </div>
        )}
        {selectedYes ? (
          <div className="email-signup__input">
            <p>Great! Please enter your email below.</p>
            <div className="email-signup__input-form">
              <input onChange={event => this.emailInput(event)} type="email" placeholder="me@example.com" />
              <button
                className="button button--blue button--round"
                onClick={() => this.handlePressSend()}
              >Send</button>
            </div>
          </div>
        ) : null}
        {selectedNo ? (
          <p>No problem, thanks for letting me know! <span role="img" aria-label="Smiling face">🙂</span></p>
        ) : null}
        {invalidEmail ? (
          <span className="email-signup__email-invalid">
            That doesn&#39;t look like a valid email address
          </span>
        ) : null }
      </div>
    );
  }
  
  handleQuestionResponse(response) {
    switch(response) {
      case 'yes':
        return this.setState({ selectedYes: true });
      case 'no':
        return this.setState({ selectedNo: true });
    }
  }
  
  emailInput(event) {
    const { target: { value }} = event;
    this.setState({ emailField: value });
  }
  
  handlePressSend() {
    const { emailField } = this.state;
    const { listName } = this.props;
    if (! emailField) {
      return;
    }
    postToList(emailField, listName).then(({ invalidEmail }) => {
      if (invalidEmail) {
        this.setState({
          invalidEmail: true,
        });
        return;
      }
      this.setState({
        pressedSend: true,
        invalidEmail: false,
        emailField: '',
      });
    });
  }
}

EmailSignupMessage.propTypes = {
  onHide: PropTypes.func,
  onShow: PropTypes.func,
  emailSignup: PropTypes.object,
  listName: PropTypes.string,
}

const mapStateToProps = ({ app: { emailSignup } }) => ({
  emailSignup
});

const mapDispatchToProps = dispatch => ({
  onHide: () => dispatch( hideEmailSignup() ),
  onShow: () => dispatch( showEmailSignup() ),
});

export default connect(mapStateToProps, mapDispatchToProps)(EmailSignupMessage);

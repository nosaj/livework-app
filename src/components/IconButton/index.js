import React from 'react';
import PropTypes from 'prop-types';
import noop from 'helpers/noop';

import './icon-button.scss';

const IconButton = ({ className, children, icon, href, onClick, target }) => 
  href ? (
    <a 
      href={href}
      onClick={() => onClick ? onClick() : noop()}
      className={`${className || ''} icon-button ${icon ? `icon-button--${icon}` : ''}`}
      target={target || '_self'}
    >{children}</a>
  ) : (
    <button 
      onClick={() => onClick ? onClick() : noop()}
      className={`${className || ''} icon-button ${icon ? `icon-button--${icon}` : ''}`}
    >{children}</button>
  );

IconButton.propTypes = {
  children: PropTypes.node,
  onClick: PropTypes.func,
  icon: PropTypes.string,
  href: PropTypes.string,
  target: PropTypes.string,
  className: PropTypes.string,
};

export default IconButton;

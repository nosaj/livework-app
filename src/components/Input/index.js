import React from 'react';
import PropTypes from 'prop-types';

import './input.scss';

const Input = props => {
  // Fancy way of extending the className of the field
  const proxyClassName = props.className ? props.className.split(' ') : [];
  proxyClassName.push('input-field');
  if (props.invalid) {
    proxyClassName.push('input-field--invalid');
  }
  return (
    <div className={proxyClassName.join(' ')}>
      <label 
        className="input-field__label"
        htmlFor={props.name}
      >{props.label} {props.optional && '(optional)'}</label>
      <input
        className="input-field__input" 
        name={props.name}
        type={props.type}
        value={props.value}
        onChange={props.onChange}
      />
      {props.invalid && props.invalidMessage && (
        <span className="input-field__invalid-message">{props.invalidMessage}</span>
      )}
    </div>
  );
}

Input.propTypes = {
  label: PropTypes.string,
  name: PropTypes.string,
  type: PropTypes.string.isRequired,
  value: PropTypes.string,
  onChange: PropTypes.func,
  optional: PropTypes.bool,
  className: PropTypes.string,
  invalid: PropTypes.bool,
  invalidMessage: PropTypes.string,
}

export default Input;

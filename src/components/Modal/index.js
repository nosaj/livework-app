import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { hideModal } from 'actions';
import X from 'components/X';

import './modal.scss';

const handleModalWindowClick = event => {
  event.stopPropagation();
}

const Close = () => (
  <div className="modal__close">
    <X />
  </div>
);

const Modal = ({ children, showing, onClose, className }) => ! showing ? null : (
  <section 
    className="modal" 
    role="presentation"
    onClick={() => onClose()}>
    <div className="modal__background"></div>
    <Close />
    <div 
      onClick={event => handleModalWindowClick(event)} 
      className="modal__contents"
      role="presentation"
      >
        {React.cloneElement(children, { onModalClose: onClose, className: `${className} modal-frame` })}
    </div>
  </section>
);

Modal.propTypes = {
  children: PropTypes.node,
  showing: PropTypes.bool,
  onClose: PropTypes.func,
};

const mapDispatchToProps = dispatch => ({
  onClose: () => dispatch( hideModal() ),
})

export default connect(undefined, mapDispatchToProps)(Modal);

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './picture-gallery.scss';

class PictureGallery extends Component {
  render() {
    const { pictures } = this.props;
    return (
      <div className="picture-gallery">
        {pictures.map((p, i) => 
          <img src={p} alt="" key={i} />
        )}
      </div>
    )
  }
}

PictureGallery.propTypes = {
  pictures: PropTypes.array,
}

export default PictureGallery;

import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import mapboxgl from 'mapbox-gl';

import PropertyMarker from 'components/PropertyMarker';
import { mapboxToken } from 'constants/app';

import './properties-map.scss';

mapboxgl.accessToken = mapboxToken;

let _mapContainer = undefined;

const centerOnLngLat = (map, lngLat) => {
  // Center the map on the selected marker
  map.easeTo({
    // Adjust center point to be just above center to compensate for 
    // the popover
    center: [lngLat[0], (lngLat[1] - 0.001)],
    zoom: 16,
  });
}

const addPropertiesToMap = (map, properties, onPropertyFocus, focussedProperty) => {
  const instantiateMarker = (markerDOM, location_lnglat) => {
    const marker = new mapboxgl.Marker(markerDOM);
    marker
      .setLngLat(location_lnglat)
      .addTo(map);
  }
  
  properties.forEach(p => {
    const markerParent = document.createElement('div');  
    ReactDOM.render(
      <PropertyMarker 
        onClick={() => {
          onPropertyFocus(p.uuid);
          centerOnLngLat(map, p.location_lnglat);
        }}
        property={p} 
        ref={() => instantiateMarker(markerParent, p.location_lnglat)} 
      />, 
      markerParent
    );
  });
  
  // When a property is focussed in the UI, center the map on it. This is useful
  // for when the page loads with a property already focussed
  if (focussedProperty) {
    centerOnLngLat(map, focussedProperty.location_lnglat);
  }
}

const instantiateMap = (container, properties, onPropertyFocus, focussedProperty, onLoaded) => {
  if (_mapContainer) {
    return;
  }
  // Store the container element outside the scope so we can differentiate between
  // initial render and state updates.
  _mapContainer = container;
  
  const london = [-0.118092, 51.509865];
  const map = new mapboxgl.Map({
    container: _mapContainer,
    center: london,
    style: 'mapbox://styles/mapbox/dark-v9',
    zoom: 11,
    dragRotate: false,
    pitchWithRotate: false, 
    minZoom: 10,
  });
  map.on('load', () => {
    const propertiesWithLngLat = properties.filter(p => p.location_lnglat.length > 0);
    addPropertiesToMap(map, propertiesWithLngLat, onPropertyFocus, focussedProperty);
    onLoaded();
  });
}

const PropertiesMap = ({ onLoaded, properties, onPropertyFocus, focus }) => {
  return (
    <div 
      className={`properties-map ${focus ? 'properties-map--disable' : ''}`} 
      ref={el => instantiateMap(el, properties, onPropertyFocus, focus, onLoaded)} 
    />
  );
};

PropertiesMap.propTypes = {
  onLoaded: PropTypes.func,
  onPropertyFocus: PropTypes.func,
  focus: PropTypes.object,
  properties: PropTypes.array,
};

export default PropertiesMap;

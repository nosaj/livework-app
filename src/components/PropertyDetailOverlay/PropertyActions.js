import React from 'react';
import PropTypes from 'prop-types';
import IconButton from 'components/IconButton';

const PropertyActions = ({ onBookViewing, onShare, property }) => (
  <section className="property-actions">
    {/* <IconButton onClick={() => onShare()} icon="share">Share</IconButton> */}
    {property.agent_email ? [
      <IconButton className="property-actions__action" key="viewing-btn" onClick={() => onBookViewing()} icon="viewing">Book a viewing</IconButton>,
      <IconButton className="property-actions__action" key="agent-btn" target="_blank" href={property.url} icon="link">Goto Agent</IconButton>
    ] : (
      <IconButton className="property-actions__action" href={property.url} target="_blank" icon="link">View on {property.agent_name}</IconButton>
    )}
  </section>
);

PropertyActions.propTypes = {
  onBookViewing: PropTypes.func,
  onShare: PropTypes.func,
  property: PropTypes.object,
}

export default PropertyActions;

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { showModal } from 'actions';
import { addressString, numberWithCommas, propertyUrl } from 'helpers/string';
import { calculateMonthPrice } from 'helpers/prices';

import TransportLinks from 'components/TransportLinks';
import PropertyGallery from 'components/PropertyGallery';
import BookViewingModal from 'components/BookViewingModal';
import ShareModal from 'components/ShareModal';
import PropertyActions from './PropertyActions';

import './property-detail-overlay.scss';

const renderViewingModal = (property, onModalOpen) => {
  const modalChildren = <BookViewingModal property={property} />
  onModalOpen(modalChildren);
}

const renderShareModal = (property, onModalOpen) => {
  const shareUrl = propertyUrl(property.uuid);
  const modalChildren = <ShareModal url={shareUrl} />
  onModalOpen(modalChildren);
}

const PropertyDetailOverlay = ({ property, modal, onModalOpen, onClose }) => {
  const monthPrice = calculateMonthPrice(property.week_price);
  return (
    <article 
      role="presentation"
      onClick={() => onClose()}
      className={`property-detail ${modal.showing ? 'freeze-scroll' : ''}`}>
      <div role="presentation" onClick={event => event.stopPropagation()}className="property-detail__content">
        <header className="property-detail__header">
          <span className="property-address">{addressString(property)}</span>
          <h1 className="property-title">{property.listing_title}</h1>
          <PropertyActions
            property={property}
            onShare={() => renderShareModal(property, onModalOpen)}
            onBookViewing={() => renderViewingModal(property, onModalOpen)} />
        </header>
        <div className="property-detail__body">
          <div className="property-detail__info">
            <div className="detail-unit">
              <h2 className="detail-unit__title">Price</h2>
              <div className="detail-unit__data">
                £{numberWithCommas(property.week_price)}<span className="detail-unit__postfix">/week</span>
              </div>
              <div className="detail-unit__note">That&#39;s &pound;{numberWithCommas(monthPrice)}/month</div>
            </div>
            {property.dimensions_sqft && (
              <div className="detail-unit detail-unit--divide">
                <h2 className="detail-unit__title">Size</h2>
                <div className="detail-unit__data">
                  {numberWithCommas(property.dimensions_sqft)}
                  <span className="detail-unit__postfix">sqft</span>
                </div>
              </div>
            )}
            {property.nearest_tube && (
              <div className="detail-unit detail-unit--wide">
                <h2 className="detail-unit__title">Transport Links</h2>
                <div className="detail-unit__data tube">
                  <strong className="tube__station-name">{property.nearest_tube.station}</strong>
                  <span className="tube__walking-time">{property.nearest_tube.estimated_walking_time} minute walk</span>
                  {property.nearest_tube.lines.length === 0 || 
                    <TransportLinks 
                      modes={property.nearest_tube.modes}
                      lines={property.nearest_tube.lines} 
                    />
                  }
                </div>
              </div>
            )}
          </div>
          <div className="property-detail__gallery">
            <PropertyGallery pictures={property.pictures} />
          </div>
        </div>
      </div>
    </article>
  )
}

PropertyDetailOverlay.propTypes = {
  property: PropTypes.object,
  modal: PropTypes.object,
  onClose: PropTypes.func,
  onModalOpen: PropTypes.func,
}

const mapDispatchToProps = dispatch => ({
  onModalOpen: children => dispatch( showModal(children) ),
});

export default connect(undefined, mapDispatchToProps)(PropertyDetailOverlay);

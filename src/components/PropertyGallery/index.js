import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './property-gallery.scss';

class PropertyGallery extends Component {
  constructor() {
    super();
    this.state = {
      activeIndex: 0,
    }
  }
  
  render() {
    const { pictures } = this.props;
    const { activeIndex } = this.state;
    return (
      <div className="property-gallery">
        {pictures.map((p, i) => (
          <img src={p} key={i} alt="" className={i === activeIndex ? 'show' : ''} />
        ))}
        {pictures.length > 1 && (
          <div className="property-gallery__controls">
            <div 
              className="property-gallery__back"
              onClick={() => this.handleBackwards()}
              ></div>
              <div 
                className="property-gallery__forwards"
                onClick={() => this.handleForwards()}
              ></div>
            </div>
        )}
      </div>
    );
  }
  
  handleForwards() {
    const { activeIndex } = this.state;
    const itemCount = this.props.pictures.length;
    if (activeIndex + 2 <= itemCount) {
      this.setState({ activeIndex: activeIndex + 1 });
      return;
    }
    this.setState({ activeIndex: 0 });
  }
  
  handleBackwards() {
    const { activeIndex } = this.state;
    const itemCount = this.props.pictures.length;
    if (activeIndex - 1 >= 0) {
      this.setState({ activeIndex: activeIndex - 1 });
      return;
    }
    this.setState({ activeIndex: itemCount - 1 });
  }
}

PropertyGallery.propTypes = {
  pictures: PropTypes.array,
}

export default PropertyGallery;

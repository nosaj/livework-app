import React from 'react';
import PropTypes from 'prop-types';
import { numberWithCommas } from 'helpers/string';

import './property-marker.scss';

const PropertyMarker = ({ onClick,  property: { location_street, week_price, dimensions_sqft }}) => {
  return (
    <article className="property-marker" onClick={onClick}>
      <div className="property-marker__contents">
        <div className="property-marker__price"><em>£{numberWithCommas(week_price)}</em>/wk</div>
        {dimensions_sqft ? (
          <div className="property-marker__secondary">
            <div className="property-marker__sqft">{numberWithCommas(dimensions_sqft)}sqft</div>
          </div>
        ) : null}
      </div>
    </article>
  );
}

PropertyMarker.propTypes = {
  onClick: PropTypes.func,
  property: PropTypes.shape({
    location_street: PropTypes.string,
    week_price: PropTypes.number,
    dimensions_sqft: PropTypes.number,
  }),

}

export default PropertyMarker;

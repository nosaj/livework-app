import React, { Component } from 'react';
import PropTypes from 'prop-types';

class SVG extends Component {
  constructor() {
    super();
    this.state = {
      svgString: null,
    }
  }
  
  componentWillMount() {
    const { src } = this.props;
    fetch(src).then(res => {
      res.text().then(text => {
        this.setState({ svgString: text });
      });
    });
  }
  
  render() {
    const { svgString } = this.state;
    const { className } = this.props;
    return <div 
        className={`${className} svg`}
        dangerouslySetInnerHTML={{__html: svgString}} 
      />
  }
}

SVG.propTypes = {
  src: PropTypes.string,
  className: PropTypes.string,
}

export default SVG;

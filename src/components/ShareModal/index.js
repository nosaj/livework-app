import React, {Component} from 'react';
import PropTypes from 'prop-types';
import './share-modal.scss';

class ShareModal extends Component {
  render() {
    return (
      <div className="share-modal"></div>
    )
  }
}

ShareModal.propTypes = {
  url: PropTypes.string,
}

export default ShareModal;

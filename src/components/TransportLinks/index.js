import React from 'react';
import PropTypes from 'prop-types';
import { tubeLines } from 'constants/transport';

import './transport-links.scss';

const Line = ({ line }) => (
  <div 
    className="tube-line" 
    style={{ backgroundColor: line.color, color: line.textColor }}
    >{line.name} Line</div>
);

Line.propTypes = {
  line: PropTypes.object,
};

const Busses = () => (
  <div className="busses">
    Busses
  </div>
);

const Overground = () => (
  <div className="overground">
    Overground
  </div>
);

const NationalRail = () => (
  <div className="national-rail">
    National Rail
  </div>
);

const TransportLinks = ({ modes, lines }) => {
  const hasTube = modes.some(m => m === 'tube');
  const hasNationalRail = modes.some(m => m === 'national-rail');
  const hasBus = modes.some(m => m === 'bus');
  const hasOverground = modes.some(m => m === 'overground');
  let lineElements;
  if (hasTube) {
    lineElements = lines.map((line, i) => {
      const lineInfo = tubeLines.filter(tl => tl.name === line)[0];
      if (! lineInfo) return null;
      return <Line key={i} line={lineInfo} />
    })
  }
  return (
    <article className="transport-links">
      {hasTube && (
        <div className="tube-lines">
          {lineElements}
        </div>
      )}
      {hasBus && <Busses />}
      {hasNationalRail && <NationalRail />}
      {hasOverground && <Overground />}
    </article>
  );
}

TransportLinks.propTypes = {
  modes: PropTypes.array,
  lines: PropTypes.array,
};

export default TransportLinks;

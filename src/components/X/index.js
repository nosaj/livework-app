import React from 'react';
import PropTypes from 'prop-types';

import './x.scss';

const X = ({ className, onClick }) => (
  <div onClick={() => onClick()} className={`x-icon ${className || ''}`}>
    <span className="x-icon__a"></span>
    <span className="x-icon__b"></span>
  </div>
);

X.propTypes = {
  onClick: PropTypes.func,
  className: PropTypes.string,
};

export default X;

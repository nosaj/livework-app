// App
export const SHOW_MODAL = 'SHOW_MODAL';
export const HIDE_MODAL = 'HIDE_MODAL';
export const HIDE_EMAIL_SIGNUP = 'HIDE_EMAIL_SIGNUP';
export const SHOW_EMAIL_SIGNUP = 'SHOW_EMAIL_SIGNUP';

// Map
export const MAP_LOADED = 'MAP_LOADED';

// Search
export const FOCUS_PROPERTY = 'FOCUS_PROPERTY';

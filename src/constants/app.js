export const appUrl = process.env.APP_URL;
export const apiUrl = `${appUrl}/api`;
export const version = process.env.npm_package_version;
export const mapboxToken = process.env.MAPBOX_API_TOKEN;

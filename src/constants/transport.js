export const tubeLines = [
  {
    name: 'Bakerloo',
    color: '#b26300',
    textColor: 'white',
    icon: null,
  },
  {
    name: 'Central',
    color: '#dc241f',
    textColor: 'white',
    icon: null,
  },
  {
    name: 'Circle',
    color: '#ffd329',
    textColor: 'black',
    icon: null,
  },
  {
    name: 'District',
    color: '#007d32',
    textColor: 'white',
    icon: null,
  },
  {
    name: 'Hammersmith & City',
    color: '#f4a9be',
    textColor: 'black',
    icon: null,
  },
  {
    name: 'Jubilee',
    color: '#a1a5a7',
    textColor: 'white',
    icon: null,
  },
  {
    name: 'Metropolitan',
    color: '#9b0058',
    textColor: 'white',
    icon: null,
  },
  {
    name: 'Northern',
    color: '#000',
    textColor: 'white',
    icon: null,
  },
  {
    name: 'Piccadilly',
    color: '#0019a8',
    textColor: 'white',
    icon: null,
  },
  {
    name: 'Victoria',
    color: '#0098d8',
    textColor: 'white',
    icon: null,
  },
  {
    name: 'Waterloo & City',
    color: '#93ceba',
    textColor: 'black',
    icon: null,
  },
];

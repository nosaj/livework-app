import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { hideModal } from 'actions';
import Router from 'router';
import EmailSignupMessage from 'components/EmailSignupMessage';
import Modal from 'components/Modal';

import './app.scss';

const GlobalElements = ({ modal }) => (
  <div className="super-elements">
    <EmailSignupMessage key={0} listName="newProperties" />
    <Modal key={1} {...modal} />
  </div>
)

GlobalElements.propTypes = {
  modal: PropTypes.object,
}

class App extends React.Component {  
  componentDidMount() {
    this.hideModalOnEscape();
  }
  
  render() {
    const { map, app } = this.props;
    return (
      <div className="app-container">
        <Router />
        {map.isLoading ? null : <GlobalElements {...app} /> }
      </div>
    );
  }
  
  hideModalOnEscape() {
    const { onModalClose } = this.props;
    document.addEventListener('keydown', event => {
      if (event.key === 'Escape') {
        onModalClose();
      }
    })
  }
}

App.propTypes = {
  app: PropTypes.object,
  map: PropTypes.object,
  onModalClose: PropTypes.func,
}

const mapStateToProps = ({ app, map }) => ({ app, map });

const mapDispatchToProps = dispatch => ({
  onModalClose: () => dispatch( hideModal() ),
})

export default connect(mapStateToProps, mapDispatchToProps)(App);

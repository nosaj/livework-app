import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import { focusProperty, mapLoaded } from 'actions';
import { getAllProperties } from 'services/api';
import { gaEvent } from 'helpers/analytics';

import SVG from 'components/SVG';
import AppHeader from 'components/AppHeader';
import PropertiesMap from 'components/PropertiesMap';
import PropertyDetailOverlay from 'components/PropertyDetailOverlay';

import './search.scss';

const MapLoader = () => (
  <div className="search-container__loading">
    <div className="buildings">
      <span className="buildings__1">
        <SVG src="/assets/artwork/buildings--1.svg" />
      </span>
      <span className="buildings__2">
        <SVG src="/assets/artwork/buildings--2.svg" />
      </span>
      <span className="buildings__3">
        <SVG src="/assets/artwork/buildings--3.svg" />
      </span>
      <span className="buildings__4">
        <SVG src="/assets/artwork/buildings--4.svg" />
      </span>
      <span className="buildings__5">
        <SVG src="/assets/artwork/buildings--5.svg" />
      </span>
    </div>
    <div className="search-container__loading-text">
      Loading properties
    </div>
  </div>
);

class SearchContainer extends Component {
  constructor() {
    super();
    this.state = {
      properties: [],
    };
  }
  
  componentWillMount() {
    getAllProperties()
      .then(properties => {
        this.setState({ properties });
      });
  }
  
  render() {
    const { map, onMapLoaded, match, modal } = this.props;
    const { properties } = this.state;
    if (! properties.length) return null;
    const availableProperties = properties.filter(p => p.available);
    // When the url changes to focus on a property, show the focus UI
    const isFocussed = match.params.hasOwnProperty('uuid');
    const focusItem = availableProperties.filter(p => p.uuid === match.params.uuid);
    return (
      <main className={`search-container ${map.isLoading ? 'search-container--is-loading' : ''}`}>
        <AppHeader />
        {map.isLoading && <MapLoader />}
        {availableProperties.length > 0 && (
          <PropertiesMap 
            focus={isFocussed ? focusItem[0] : null} 
            onPropertyFocus={uuid => this.focusOnProperty(uuid)}
            onLoaded={() => onMapLoaded()}
            properties={availableProperties} />
        )}
        {(isFocussed && ! map.isLoading) && (
          <PropertyDetailOverlay
            property={focusItem[0]}
            modal={modal}
            onClose={() => this.returnToMap()}
            />
        )}
      </main>
    )
  }
  
  focusOnProperty(uuid) {
    const { history } = this.props;
    gaEvent('select', 'markers', uuid);
    history.push(`/properties/${uuid}`);
  }
  
  returnToMap() {
    const { history } = this.props;
    history.push('/');
  }
}

SearchContainer.propTypes = {
  history: PropTypes.object,
  match: PropTypes.object,
  search: PropTypes.object,
  map: PropTypes.object,
  modal: PropTypes.object,
  onPropertyBlur: PropTypes.func,
  onMapLoaded: PropTypes.func,
}

const mapDispatchToProps = dispatch => ({
  onMapLoaded: () => dispatch( mapLoaded() ),
  onPropertyBlur: () => dispatch( focusProperty(null) ),
});

const mapStateToProps = ({ search, map, app: { modal } }) => ({
  modal,
  search,
  map
});

export default connect(mapStateToProps, mapDispatchToProps)(
  withRouter(SearchContainer)
);

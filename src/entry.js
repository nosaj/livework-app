// Global imports / polyfills
import 'whatwg-fetch';

// Load global styles and index.html file
import './styles/livework.scss';

import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import App from 'containers/App';
import reducer from './reducers';

const store = createStore(reducer);

render((
  <Provider store={store}>
    <App />
  </Provider>
), document.getElementById('app'));

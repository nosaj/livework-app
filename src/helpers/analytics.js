
// Proxy for sending events to google analytics
export const gaEvent = (name, category, label) => {
  if (! window.hasOwnProperty('gtag')) {
    console.warn('Not sending events to GA gtag.')
    return;
  }
  gtag('event', name, {
    event_category: category, 
    event_label: label
  });
}

export const calculateMonthPrice = weekPrice => Math.round((weekPrice * 52) / 12);

import { appUrl } from 'constants/app';

// Compute the address string from a property object
export const addressString = ({
  location_postcode,
  location_street, 
  location_area,
  location_city
}) => {
  let str = '';
  str += location_street ? `${location_street}, ` : ''; 
  str += location_area ? `${location_area}, ` : ''; 
  str += location_postcode ? `${location_postcode} , ` : ''; 
  str += location_city ? `${location_city}` : ''; 
  return str;
}

export const numberWithCommas = n => {
  if (! n) {
    console.error('Couldn\'t add commas, undefined');
    return null;
  }
  var parts = n.toString().split(".");
  parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  return parts.join(".");
}

export const propertyUrl = uuid => `${appUrl}/properties/${uuid}`;

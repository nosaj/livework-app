import { combineReducers } from 'redux';
import * as types from 'constants/actionTypes';

const initialState = {
  map: {
    isLoading: true,
  },
  app: {
    isLoading: true,
    modal: {
      showing: false,
      children: null,           // This is just the React children, for the modal body
    },
    emailSignup: {
      isHidden: true,           // So it's not annoying, only show it when the app knows that the user hasn't already dismissed it
    }
  },
  search: {
    propertyFocus: null,        // Which property is focussed in the UI?  
  }
};


const app = (state = initialState.app, action) => {
  switch(action.type) {
    case types.HIDE_MODAL:
      return Object.assign({}, state, { modal: { showing: false, children: null }});
    case types.SHOW_MODAL:
      return Object.assign({}, state, { modal: { showing: true, children: action.children } });
    case types.SHOW_EMAIL_SIGNUP:
      return Object.assign({}, state, { emailSignup: { isHidden: false }});
    case types.HIDE_EMAIL_SIGNUP:
      return Object.assign({}, state, { emailSignup: { isHidden: true }});
    default: 
      return state;
  }
}

const map = (state = initialState.map, action) => {
  switch(action.type) {
    case types.MAP_LOADED:
      return Object.assign({}, state, { isLoading: false });
    default:
      return state;
  }
}

const search = (state = initialState.search, action) => {
  switch(action.type) {
    case types.FOCUS_PROPERTY:
      return Object.assign({}, state, { propertyFocus: action.property });
    default: 
      return state;
  }
}

export default combineReducers({
  app,
  map,
  search
});

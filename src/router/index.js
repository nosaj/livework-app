import React, { Component } from 'react';
import { Router, Route, Switch } from 'react-router'
import createHistory from 'history/createBrowserHistory';
import Search from 'containers/Search';

class AppRouter extends Component {
  shouldComponentUpdate() {
    // Never update. router only needs to be declared once
    return false;
  }
    
  render() {
    return (
      <Router history={createHistory()}>
        <Switch>
          <Route exact path="/" component={Search} />
          <Route path="/properties/:uuid" component={Search} />
        </Switch>
      </Router>
    );
  }
}

export default AppRouter;

import { apiUrl } from 'constants/app';
import { bodyString } from 'helpers/api';

const apiRequest = async ({ url, method='get', body }) => {
  const opts = {
    method, 
    body: bodyString(body),
    mode: 'cors',
    cache: 'default',
    credentials: 'same-origin',
  };
  if (body) {
    const requestHeaders = new Headers();
    requestHeaders.append('Content-Type', 'application/json');
    opts.headers = requestHeaders;
  }
  const result = await fetch(url, opts);
  return await result.json();
}

// Return all properties from the server
export const getAllProperties = async () => {
  const request = {
    url: `${apiUrl}/properties`,
  };
  const properties = await apiRequest(request);
  return properties;
}

export const getSearch = async () => {
  
}

// Find out which lists the user has signed up to based on a 
// cookie set by the server.
export const getUserLists = async () => {
  const request = {
    url: `${apiUrl}/lists`,
  };
  const lists = await apiRequest(request);
  return lists;
}

// Post data to a list
export const postToList = async (email, list, data={}) => {
  const request = {
    url: `${apiUrl}/lists`,
    method: 'post',
    body: { email, list, data }
  }
  const result = await apiRequest(request);
  return result;
}

export const postBooking = async (name, email, phone, availability, listingUrl, listingTitle, agentId) => {
  if (! name || ! email || ! availability || ! listingUrl || ! listingTitle || ! agentId) {
    throw new Error('Post to /bookings: missing param(s) from post body');
  }
  const request = {
    url: `${apiUrl}/bookings`,
    method: 'post',
    body: { name, email, availability, phone, listingUrl, listingTitle, agentId },
  }
  const result = await apiRequest(request);
  return result;
}
